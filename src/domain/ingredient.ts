export type Ingredient = {
  id: string;
  title: string;
};
